<?php

namespace Hijauasri\Eloquent;

class Subscriber extends Model
{
    protected $fillable = ['email', 'sent'];
}